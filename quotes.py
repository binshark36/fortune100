#!/usr/bin/env python
import easyquotation
import time
import sys
import curses
stocks = ['sh000001','510300','600486','600309','002465','600674','600886']
interval = 2
quotation = easyquotation.use('sina')
stdscr = curses.initscr()
curses.curs_set(0) ## hide cursor

def main(stdscr):
	while True:
		stdscr.refresh()
		quo_ret = quotation.real(stocks)
		## special handle for sh00001
		## should be a better way. Too lazy to find out
		for i in range(len(stocks)):
			if stocks[i] == 'sh000001':
				out_line = stocks[i] + " " + str(quo_ret['000001']['now'])
			else:
				out_line = stocks[i] + " " + str(quo_ret[stocks[i]]['now'])
			#stdscr.addstr(out_line)
			stdscr.clrtoeol()
			print(out_line,"\r")
		time.sleep(interval)
		stdscr.clear()

curses.wrapper(main)
