#!/usr/bin/env python

## ×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
##
##  由于上交所的基金交易量很小，滑点率大，即使脚本提示有套利空间，实际也难以成交
##  ，据此脚本进行套利所产生的任何后果，无论经济的还是其它方面的，本人概不负责。
##
## ×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××

## 这个脚本是用于计算上交所分级基金的分拆/合并套利空间
## 要使用时直接运行python3 shanghai_fenji.py即可，要结束时就按crtl^C来结束

## 输出示例（有套利空间时)：
## ---------------------------------------------------------
## |  分拆套利  |    买入母基   |    卖出A    |    卖出B   |
## +------------+---------------+-------------+------------+
## |  50 分级   |    502048     |   502049    |   502050   |
## +------------+---------------+-------------+------------+
## | 167 /  117 |     1.096     |   1.020     |    1.180   |
## ---------------------------------------------------------

## 如果没有，脚本就输出一行显示当前时间信息。

## 这个脚本依赖主要依赖easyquotation这个模块，而这个模块的安装需要python3.5+
## 详见 https://github.com/shidenggui/easyquotation
## 除此之外，还需要安装colorama模块 (pip install colorama),用于调整输出的颜色

import time
import easyquotation
from colorama import Fore,Back,Style


## 目前并没有加入所有的分级基金，主要是有些基金交易量太小的问题
SH_FUNDS = {
        'silkroad': {'code':'502013','name':'一带一路','A_code':'502014','B_code':'502015','buy_comm':0.012,'redemp_comm':0.005},
        'brokers': {'code':'502053','name':'券商分级','A_code':'502054','B_code':'502055','buy_comm':0.012,'redemp_comm':0.005},
        'securities': {'code':'502010','name':'证券分级','A_code':'502011','B_code':'502012','buy_comm':0.0,'redemp_comm':0.005},
        'SH50': {'code':'502048','name':'50分级','A_code':'502049','B_code':'502050','buy_comm':0.0,'redemp_comm':0.005},
        'GQGG': {'code':'502006','name':'国企改革','A_code':'502007','B_code':'502008','buy_comm':0.0,'redemp_comm':0.005},
        'military': {'code':'502003','name':'军工分级','A_code':'502004','B_code':'502005','buy_comm':0.0,'redemp_comm':0.005},
        'steel': {'code':'502023','name':'钢铁分级','A_code':'502024','B_code':'502025','buy_comm':0.0,'redemp_comm':0.005},
        'beltroad': {'code':'502016','name':'带路分级','A_code':'502017','B_code':'502018','buy_comm':0.012,'redemp_comm':0.005},
        'medicare': {'code':'502056','name':'医疗分级','A_code':'502057','B_code':'502058','buy_comm':0.0,'redemp_comm':0.005},
        'netfin': {'code':'502036','name':'互联金融','A_code':'502037','B_code':'502038','buy_comm':0.0,'redemp_comm':0.005},
        'highrail': {'code':'502030','name':'高铁分级','A_code':'502031','B_code':'502032','buy_comm':0.006,'redemp_comm':0.005},
        
        }

## 所有要获取行情信息的基金代码。母基/A/B,以及上证指数000001
all_codes = ['000001']
for name,fund in SH_FUNDS.items():
    all_codes.extend([fund['code'],fund['A_code'],fund['B_code']])

#all_codes = ['502053','502054','502055','502048','502049','502050','502003','502004','502005','502006','502007','502008','502013','502014','502015','502010','502011','502012','000001']
broker_commission = 0.0003
profit_min = 100
profit_lead_to_action = 300

quo_lf = easyquotation.use('sina')

while True:
    all_quotes = quo_lf.stocks(all_codes) 
    for name,fund in SH_FUNDS.items():
        quote_base_sell1 = all_quotes[fund['code']]['ask1']
        quote_base_sell1_volume = all_quotes[fund['code']]['ask1_volume']
        quote_base_buy1 = all_quotes[fund['code']]['bid1']
        quote_base_buy1_volume = all_quotes[fund['code']]['bid1_volume']
        quote_base_now = all_quotes[fund['code']]['now']
        quote_base_now_volume = all_quotes[fund['code']]['volume']

        quote_A_sell1 = all_quotes[fund['A_code']]['ask1']
        quote_A_sell1_volume = all_quotes[fund['A_code']]['ask1_volume']
        quote_A_buy1 = all_quotes[fund['A_code']]['bid1']
        quote_A_buy1_volume = all_quotes[fund['A_code']]['bid1_volume']
        quote_A_now = all_quotes[fund['A_code']]['now']
        quote_A_now_volume = all_quotes[fund['A_code']]['volume']

        quote_B_sell1 = all_quotes[fund['B_code']]['ask1']
        quote_B_sell1_volume = all_quotes[fund['B_code']]['ask1_volume']
        quote_B_buy1 = all_quotes[fund['B_code']]['bid1']
        quote_B_buy1_volume = all_quotes[fund['B_code']]['bid1_volume']
        quote_B_now = all_quotes[fund['B_code']]['now']
        quote_B_now_volume = all_quotes[fund['B_code']]['volume']

        ## A/B或母基的最小成交量
        min_volume = min([quote_base_now_volume,quote_A_now_volume,quote_B_now_volume])
        # 买一的量（如果任一个品种的买一为0，则意味着跌停状态）
        min_buy1_volume = min([quote_A_buy1_volume,quote_B_buy1_volume,quote_base_buy1_volume])

        ## 计算合并溢价
        merge_profit = (quote_base_now * 50000 * (1 - broker_commission)) - ((quote_A_now + quote_B_now) * 25000 *  (1 + broker_commission))

        ## 买时以卖一价买入，卖时以买一价卖出。以此方式计算合并套利空间
        min_merge_profit = (quote_base_buy1 * 50000 * (1 - broker_commission)) - ((quote_A_sell1 + quote_B_sell1) * 25000 *  (1 + broker_commission))

        split_profit = ((quote_A_now + quote_B_now) * 25000 *  (1 - broker_commission)) - (quote_base_now * 50000 * (1 + broker_commission)) 
        ## 买时以卖一价买入，卖时以买一价卖出。以此方式计算分拆套利空间
        min_split_profit = ((quote_A_buy1 + quote_B_buy1) * 25000 *  (1 - broker_commission)) - (quote_base_sell1 * 50000 * (1 + broker_commission)) 



        #print("合并利润: ",fund['name'], "is ",merge_profit,"A 卖1", quote_A_now,"B 卖1",quote_B_now,"母基买1",quote_base_now)
        #print("分拆利润: ",fund['name'], "is ",split_profit,"A 买1", quote_A_now,"B 买1",quote_B_now,"母基卖1",quote_base_now,"\n")
        if merge_profit > profit_min and merge_profit < profit_lead_to_action and min_volume > 500000:
            print('---------------------------------------------------------')
            print("|  合并套利  |    买入A    |    买入B    |   卖出母基   |")
            print('+------------+-------------+-------------+--------------+')
            print('|  %4s  |    %s   |    %s   |    %s    |' %(fund['name'],Fore.RED + fund['A_code'] + Fore.RESET,fund['B_code'],fund['code']))
            print('+------------+-------------+-------------+--------------+')
            print('|%4.0f / %4.0f | %7.3f     |  %7.3f    |  %7.3f     |' %(merge_profit,min_merge_profit,quote_A_now,quote_B_now,quote_base_now))
            print('---------------------------------------------------------')
        if merge_profit > profit_lead_to_action and min_volume > 500000 and min_buy1_volume > 500:
            print('---------------------------------------------------------')
            print("|  合并套利  |    买入A    |    买入B    |   卖出母基   |")
            print('+------------+-------------+-------------+--------------+')
            print('|  %4s  |   %s    |    %s   |    %s    |' %(fund['name'],fund['A_code'],fund['B_code'],fund['code']))
            print('+------------+-------------+-------------+--------------+')
            print('|%4.0f / %4.0f |  %7.3f    |  %7.3f    |  %7.3f     |' %(merge_profit,min_merge_profit,quote_A_now,quote_B_now,quote_base_now))
            print('---------------------------------------------------------')

        if split_profit > profit_min and min_volume > 500000 and min_buy1_volume > 500:
            print('---------------------------------------------------------')
            print("|  分拆套利  |    买入母基   |    卖出A    |    卖出B   |")
            print('+------------+---------------+-------------+------------+')
            print('|  %4s  |    %s     |   %s    |   %s   |' %(fund['name'],Fore.RED + fund['code'] + Fore.RESET,fund['A_code'],fund['B_code']))
            print('+------------+---------------+-------------+------------+')
            print('|%4.0f / %4.0f |   %7.3f     | %7.3f     |  %7.3f   |' %(split_profit,min_split_profit,quote_base_now,quote_A_now,quote_B_now))
            print('---------------------------------------------------------')

    print("_ _ _ _ _ _ _ ",time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())) ,"_ _ _ _ _ _ _ _ \n")
    time.sleep(10)
